//importar dependencia mongoose
//Importar módulo de MongoDB =>mongoose
const mongoose=require('mongoose');

//Importar dependencia variable de entorno
//require('dotenv').config({path:'../var.env'})
require('dotenv').config({path:'var.env'})

const conexionDB=async() => {
    try {
        await mongoose.connect(process.env.URL_MONGODB,{});
        console.log("Conexión establecida con MongoDB Atlas");
        
    } catch (error) {
        console.log("Error de conexión a la base de datos");
        process.exit(1)
    }
}
module.exports = conexionDB