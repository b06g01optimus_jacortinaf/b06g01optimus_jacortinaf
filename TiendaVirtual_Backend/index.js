//Importar express - sintaxis ES6 =>estándar de JavaScript
//import express from "express" (Forma numero 1)
//Sintaxis nativa de Nodejs:const{Router}=require('express') (Forma numero 2)- cualquiera sirve.
const express = require('express');
const router = express.Router();
//Importar módulo de mongoDB => mongoose
//const mongoose=require('mongoose');


//Importé dependencias variable de entorno
//require('dotenv').config({path:'./var.env'})
const conectarDB= require('./config/db.js')

let app = express();

//Conexión base de datos
conectarDB();
//mongoose.connect(process.env.URL_MONGODB)
//        .then(function(){console.log("Conexion establecida con mongoDB Atlas");})
//        .catch(function(e){console.log(e)})

//Integración del frontend en el backend.
app.use(express.static('public'));




/* //Modelo Esquema (Schema) de la base de datos.
const productSchema = new mongoose.Schema({
    id:Number,
    nombre:String,
    cantidad:Number,
    categoria:String,
    precio:Number
}); */

/* //Modelo del producto => tener en cuenta el esquema para la colección ...
const modeloProducto = mongoose.model('modelo_prod',productSchema); */

//CRUD => Create
/* modeloProducto.create({
    id:5,
    nombre:"Arroz",
    cantidad:46,
    categoria:"Viveres",
    precio:6000
    },
    (error)=>{
        //console.log("ocurrió el siguiente error")
        if(error) return console.log(error);
        // console.log(error);
        // console.log("Fin del Error");
    }
); */

//--------------------------------------------
//#########  Descentralizar el CRUD ##########
//--------------------------------------------


// CRUD => Read
/* modeloProducto.find((error, productos) => {
    if (error) return console.log(error);
    console.log(productos);
}) */ 


/* // CRUD => Update
modeloProducto.updateOne({id:5}, {cantidad: 40}, (error) => {
    if (error) return console.log(error);
}) */

/* // CRUD => Delete
modeloProducto.deleteOne({id: 2}, (error) => {
    if (error) return console.log(error);
}); */

 
//--------------------------------------------
//#########  Rutas respecto  al CRUD ##########
//--------------------------------------------
//a tener en cuenta:1-Respecto a la aplicación los archivos json, 
//y 2- Los CORS:Política, reglas de seguridad control  de peticiones http. 


//2-CORS(Cross-Origin Resource Sharing)
const cors =require('cors');
app.use(cors());

//Solucion temporal para habilitar los cors
var whitelist = ['http://localhost:4000/', 'http://localhost:4200/']
var corsOptionsDelegate = function (req, callback) {
  var corsOptions;
  if (whitelist.indexOf(req.header('Origin')) !== -1) {
    corsOptions = { Origin: true } // reflect (enable) the requested Origin in the CORS response
  }else{
    corsOptions = { Origin: false } // disable CORS for this request
  }
  callback(null, corsOptions) // callback expects two parameters: error and options
}



//solicitudes al CRUD=> el controlador
const crudProductos = require('./controllers/controlProducts');

//1-Uso de archivos tipo json en la app:
app.use(express.json());

//Establecer las rutas respecto al CRUD:
app.use(router);
//CRUD=>Create
router.post('/apirest/', cors(corsOptionsDelegate), crudProductos.crear);
//CRUD=>Read
router.get('/apirest/', cors(corsOptionsDelegate), crudProductos.obtener);
//CRUD=>Update
router.put('/apirest/:id', cors(corsOptionsDelegate), crudProductos.actualizar); //se pasa el parametro por ruta
//CRUD=>Delete
router.delete('/apirest/:id',cors(corsOptionsDelegate), crudProductos.eliminar); //se pasa el parametro por ruta


// //Establecer las rutas respecto al CRUD:
// app.use(router);
// //CRUD=>Create
// router.post('/',crudProductos.crear);
// //CRUD=>Read
// router.get('/',crudProductos.obtener);
// //CRUD=>Update
// router.put('/:id',crudProductos.actualizar); //se pasa el parametro por ruta
// //CRUD=>Delete
// router.delete('/:id',crudProductos.eliminar); //se pasa el parametro por ruta







// app.use('/',function(req, res){
//     res.send('Hola tripulantes');
// });
//Gestor de rutas de express
// app.use(router)
// router.get('/met',function(req,res){
//     res.send("Estoy utilizando el método GET");
// });




//Gestor de rutas de express con función flecha
// app.use(router)
// router.get('/met',(req,res)=>{
//     res.send("Estoy utilizando el método GET");
// });



// router.post('/metodopost',function(req,res){
//     console.log("Antes de la conexión a la DB");
//     //Conexión de la base de datos
//     //const user = 'b06g01';
//     //const psw = 'b06g01123'
//     //const db = 'b06g01';
//     //const url = `mongodb+srv://${user}:${psw}@cluster0.todzo.mongodb.net/${db}?retryWrites=true&w=majority`;
//     //mongoose.connect(url)
//     //    .then(function(){console.log("Conexion establecida con mongoDB Atlas");})
//     //    .catch(function(e){console.log(e)})
//     console.log("Despues de la conexion a la DB")    
//     res.send("Estoy utilizando el método POST");
// });

//Conexión de la base de datos
//const user = 'b06g01';
//const psw = 'b06g01123'
//const db = 'b06g01';
//const url = `mongodb+srv://${user}:${psw}@cluster0.todzo.mongodb.net/${db}?retryWrites=true&w=majority`;
//mongoose.connect(url)
//    .then(function(){console.log("Conexion establecida con mongoDB Atlas");})
//    .catch(function(e){console.log(e)})

//Pagina web de despliegue
// app.listen(4000);
app.listen(process.env.PORT);

console.log("La aplicación se ejecuta en http://localhost:4000");

