//Importar el modelo de la base de datos para el CRUD.
const { model } = require('mongoose');
const modeloProducto =require('../models/modelProducts');

//Exportación directa: Exportar en diferentes variables los métodos para el CRUD.

//CRUD=>Create
exports.crear = async (req,res)=>{
    try {
        let producto;
        console.log("req.body:",req.body) //Para saber que esta sucediendo con el requerimiento.
        producto= new modeloProducto(req.body);//nodemon toma el contenido del archivo JSON.
        // producto = new modeloProducto({   //Instanciar con respecto a la ejecución el modelo.
        //     id:7,
        //     nombre:"Pernil",
        //     cantidad:15,
        //     categoria:"Carnes",
        //     precio:30000,
        //     urlimg:''
        //     //,fvp:"Vence: 11/11/2021" //para que searegistada esta propiedad se debe especificar en el schema-modelo.
        // });          
        
        await producto.save();  //await=>espere mientras guarda, funciona con async.

        res.send(producto);  //Respuesta - res - para usar con las rutas y probar con postman.

    } catch (error) {
        console.log(error);
        res.status(500).send('Error al guardar producto.');  //error con respecto al servidor.  
    };
    
};

//CRUD => Read
exports.obtener = async (req,res)=>{
    try {
        const producto = await modeloProducto.find();
        res.json(producto);

    } catch (error) {
        
        console.log(error);
        res.status(500).send('Error al obtener el/los producto(s).');
    }
};


//CRUD => Update
exports.actualizar = async (req,res)=>{
    try {
        
        const producto = await modeloProducto.findById(req.params.id);
        console.log(producto); //Si no se utiliza la espera anterior (await),se generan falsas respuestas del código. 
        if(!producto){
            console.log(producto);
            res.status(404).json({msg:'El producto no existe.'}); //El error 404 es cuando no encuentra un documento.

        }else{
            // await modeloProducto.findByIdAndUpdate({_id:req.params.id},{cantidad:25});
            await modeloProducto.findByIdAndUpdate({_id:req.params.id},req.body);
            res.json({msg:'Producto actualizado correctamente.'});
        }
    } catch (error) {
        console.log(error);
        res.status(500).send('Error al editar el producto.')
        
    }
}

//CRUD=>Delete

exports.eliminar =async (req, res)=>{
    try {
        const producto = await modeloProducto.findById(req.params.id);//se insiste que req.params.id es para poder especificar las rutas.Uso Postman.
        //console.log(producto);//probar la importancia de la espera y evitar falsos resultados.
        if(!producto){
            console.log(producto);
            res.status(404).json({msg:'El producto no existe.'});
        }else{
            await modeloProducto.findByIdAndRemove({_id:req.params.id});
            res.json({mensaje:'Producto eliminado.'});
        }
    } catch (error) {
        console.log(error);
        res.status(500).send('Error al eliminar el producto.')
    }
}
