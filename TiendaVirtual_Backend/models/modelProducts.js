//Importar módulo de mongoDB => mongoose
const mongoose=require('mongoose');

//Schema => estructura tipo JSON que define la estrutura de la base de datos. 
const productSchema =mongoose.Schema({
    id:String, //se pueden incluir mas opciones=>documentacion.
    nombre:String,
    cantidad:String,
    categoria:String,
    precio:String,
    urlimg:String
    //,fvp:String
},
{
    //Opciones para controlar resultados del CRUD(aparecen o no!)
    versionKey:false,
    timestamps:true

});

//Modelo del producto => tener en cuenta el esquema para la colección ...
module.exports= mongoose.model('productos',productSchema);