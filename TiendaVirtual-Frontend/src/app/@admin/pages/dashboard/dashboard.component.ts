import { Component, OnInit } from '@angular/core';
import { audit } from 'rxjs';  //a que se deberá esto?
import { ProductsService } from 'src/app/@services/products.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  //Atributos
  formEditar:boolean=false;
  _id:any;
  id:any;
  nombre:any;
  cantidad:any;
  categoria:any;
  precio:any;
  urlimg:any;
  productos:any; //Se declara para aprovechar lo que ya tenemos con ella desarrollado.
  // productos:any =[
  //   {
  //     id:1,
  //     nombre:"Lomo",
  //     cantidad:20,
  //     categoria:"Carnes",
  //     precio:20000,
  //     urlimg:"https://bit.ly/3louagL"

  //   },
  //   {
  //     id:2,
  //     nombre:"Jabón",
  //     cantidad:30,
  //     categoria:"Aseo",
  //     precio:20000,
  //     urlimg:'https://bit.ly/3louagL'

  //   }
  // ]

  //Constructores
  constructor(private _producto:ProductsService) { }  //La variable _producto permite comunicarse con el servicio CRUD =>Read


  ngOnInit(): void {
     this.obtenerProd();
  }

  //Metodos o Funciones

  // Activar Formulario Editar Producto
  editarForm(_id:any, id:any, nombre:any, cantidad:any, categoria:any, precio:any, urlimg:any){
    this.formEditar=true;
    this._id=_id;
    this.id=id;
    this.nombre=nombre;
    this.cantidad=cantidad;
    this.categoria=categoria;
    this.precio=precio;
    this.urlimg=urlimg;




  }

  cancelarEditForm(){
    this.formEditar=false;
    this._id="";
    this.id="";
    this.nombre="";
    this.cantidad="";
    this.categoria="";
    this.precio="";
    this.urlimg="";
  }

  //Guardar productos
  guardarProducto(){        //Hay que modificarlo para guardar por intermedio del backend.
    for(let i=0;i<3;i++){
      console.log("i= ",i+1);
    };
    console.log("Variable Nombre ",this.nombre); //This.nombre hace referencia a imprimir el atributo
    console.log("Variable Categoria ",this.categoria);
    if(this.nombre != undefined){
      let ind= this.productos.length+1;
      let prodAux={
        id:ind,
        nombre:this.nombre,
        cantidad:this.cantidad,
        categoria:this.categoria,
        precio:this.precio,
        urlimg:this.urlimg
      }

      this.nombre=undefined;
      this.cantidad=undefined;
      this.categoria=undefined;
      this.precio=undefined;  
      this.urlimg=undefined;

      //this.productos.push(prodAux);//arreglo de productos.
      this._producto.guardarDatos(prodAux)
      .subscribe(datos =>{
        console.log("Datos guardados:", datos);
        this.obtenerProd();
      })
    }
   }

   //Funcion Actualizar
   actualizarProducto(){
    let prodAux={
      id:this.id,
      nombre:this.nombre,
      cantidad:this.cantidad,
      categoria:this.categoria,
      precio:this.precio,
      urlimg:this.urlimg
    }
    this._producto.actualizarDatos(this._id, prodAux)
      .subscribe(datos =>{
        console.log("Producto actualizado ...", datos);
        this.obtenerProd();
      })
    this.formEditar=false;
    this._id="";
    this.id="";
    this.nombre="";
    this.cantidad="";
    this.categoria="";
    this.precio="";
    this.urlimg=""; 
   }
   
  
  // Funcion eliminar
  eliminar(indice:any){     //hay que modificarlo para eliminar con el backend.
    console.log("Elemento a eliminar",indice );
    //elimina elemento del arreglo
    this.productos.splice(indice,1);
    this._producto.eliminarDatos(indice)
        .subscribe(datos =>{
          console.log("Dato eliminado ",indice);
          this.obtenerProd();
        })
        
  }
  
  //Función para obtener productos con el backend
  
  obtenerProd(){
    this._producto.obtenerDatos() //hay que definir su subscripcion ya que es observable
    .subscribe(datos =>{
      this.productos = datos;
      console.log(this.productos);
    })
  }
    
  
}
