import {HttpClient} from '@Angular/common/http'
import { Injectable } from '@angular/core';
@Injectable({
  providedIn: 'root'
})


export class ProductsService {

  //Atributos
  // url:any='http://localhost:4000/apirest/'; //Enlace con el backend
  url:any=' https://b06g01-tiendavirtual.herokuapp.com/apirest/'; //Enlace con heroku, tiene certificado de seguridad
  //url:any=''; 

  //Constructor
  constructor(private http:HttpClient) { } //Es privado por ser una comunicacion entre clases, con el backend.

  //Metodos o funciones
  
  // CRUD => Create
  guardarDatos(producto: any){
    return this.http.post(this.url, producto);//Se hace internamente, no por la ruta.
  }

  //CRUD => Read
  obtenerDatos(){
    return this.http.get (this.url);
  
  }

  //CRUD =>Update (Combinación entre Eliminar y guardar datos)
  actualizarDatos(id:any, producto:any){
    return this.http.put(this.url+id,producto);
  }

  // CRUD => Delete
  eliminarDatos(id: any){
    return this.http.delete(this.url + id)
  }



}
