import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
//Importar el HomeComponent
import { HomeComponent } from './home.component';

const routes: Routes = [
  //Agregar la ruta - Nota: path es vacio porque es la ruta principal
  {
    path:'',
    component:HomeComponent
  }
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
