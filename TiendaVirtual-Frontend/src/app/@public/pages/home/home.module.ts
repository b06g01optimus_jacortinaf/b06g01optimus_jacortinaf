import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component'; //Se agrega al crear el componente


@NgModule({
  declarations: [
    HomeComponent
  ],
  imports: [
    CommonModule,
    HomeRoutingModule  //Si se hubiese creado primero el componente y despues el módulo, esta variable se agregaria a app.module.ts
  ]
})
export class HomeModule { } 
